﻿using UnityEngine;
using System.Collections;

public class ActivateObject : MonoBehaviour {

	// Activate a game object
	public void Init(){
		gameObject.SetActive(true);
	}
}
