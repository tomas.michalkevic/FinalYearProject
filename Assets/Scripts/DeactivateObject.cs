﻿using UnityEngine;
using System.Collections;

public class DeactivateObject : MonoBehaviour {

	// Set a game object active
	public void Init(){
		gameObject.SetActive(false);
	}
}
